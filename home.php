<!DOCTYPE html>
<head>
    <title>Community News - Home</title>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen"/>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Cantora+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'/>
    <script>
        $(".storySnippet").click(function(){
            window.location=$(this).find("a").attr("href"); 
            return false;
         });
    </script>

</head>
<body>

<div class="headerContainer">   
        <h3 class="logo"> <a href="home.php">Community News </a> </h3>
        
        <p class="welcome">
        
            <?php
                session_start();
                if (!empty($_SESSION['user'])){
                    echo "Welcome, ".$_SESSION['user'];
                }
            ?>
        
        </p>
            <form id="search" class="searchForm" action="searchResults.php" method="POST">
                <label class="searchLabel">
                    Search For:
                </label><br>
                <input type="text" class="searcharea" name="SearchFor"/>
                <input form="search" type="submit" value="Search"/>
            </form>
            
        <?php
        
            if (!empty($_SESSION['user'])){
                echo "<div id='menuLinksContainer'>        
                        <a class='menuLinks' href='newpost.php'>New Post</a>
                        <a class='menuLinks' href='mystories.php'>My Stories</a> 
                        <a class='menuLinks' class='lastmenuLinks' href='logout.php'>Log out</a>
                    </div>";
            }
            else{
                echo "<div id='menuLinksContainer'>
                        <a class='menuLinks' href='index.php'>Log In</a>
                      </div>";   
            }
        ?>

</div>

<div class="bodyContainer">
<!--Create list of 10 latest story titles that are links to the full story with comments-->
    <?php
        require "databaseAccess.php";
        //get 10 latest stories
        $stmt = $mysqli->prepare("select title, author, body, time, id from stories order by time DESC");
            $stmt->execute();
            $stmt->bind_result($title, $author, $body, $time, $story_id);
        $count = 0;
        while ($stmt->fetch() && ($count<10)){
            $count++;
            //echo $count;
            echo "<div class='storySnippet'>";
            echo "<a href=story.php?id=".$story_id.">";
                echo "<div class='storyTitle'>".$title."</div>";
                echo "<div class='storyAuthor'>".$author."</div>";
                echo "<div class='storyTime'>".$time."</div>";
                //truncate body to 300 character snippet
                $body_trunc = substr($body, 0, 300)."...";
                echo "<div class='storyBody'>".$body_trunc."</div>";
            echo "</a>";
            echo "</div>";

        }
        $stmt->close();
        
        
    ?>

</div>





<!--Button to view next 10 latest news stories-->




</div>

<div class="footer">
        
    <a href="deactivate.php">Delete account</a>
</div>

</html>