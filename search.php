<!DOCTYPE html>
<head>
    <title>Community News - Search</title>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen"/>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Cantora+One' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'/>
</head>
<body>
    <?php
        require 'databaseAccess.php';

        session_start();
        if (empty($_SESSION['user'])){
            header("Location: index.php?loginErrorWarning=q");
            exit;
        }
    ?>
<div class="headerContainer">
        <h3 class="logo"> <a href="home.php">Community News </a> </h3>
        
        <p class="welcome"> Welcome,
        
            <?php
                echo $_SESSION['user'];
            ?>
        
        </p>
                <div id="menuLinksContainer">
                    
            <a class="menuLinks" href="search.php">Search</a>
            
            <a class="menuLinks" href="newpost.php">New Post</a>
            
            <a class="menuLinks" href="mystories.php">My Stories</a> 
            
            <a class="menuLinks" class="lastmenuLinks" href="logout.php">Log out</a>
        </div>

</div>

<div class="bodyContainer">
    
    <form id="search" class="searchForm" action="searchResults.php" method="POST">
        <label class="searchLabel">
            Search For:
        </label><br>
        <input type="text" class="searcharea" name="SearchFor"/>
        <input form="search" type="submit" value="Search"/>
    </form>
    
</div>

<div class="footer">
        
    <a href="deactivate.php">Delete account</a>
</div>

</body>

</html>
