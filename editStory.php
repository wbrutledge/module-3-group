<!DOCTYPE html>
<head>
    <title>Community News - Edit Story</title>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen"/>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Cantora+One' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'/>
</head>
<body>
    <?php
        require 'databaseAccess.php';
        session_start();
        if (empty($_POST['story_id'])){
            header("Location: home.php");
            exit;
        }

        $story_id = $_POST['story_id'];
        
        //Using story ID and comment ID, retrieve info
        $stmt1 = $mysqli->prepare("select title, body from stories where id=?");
        $stmt1->bind_param('i', $story_id);
        $stmt1->execute();
        $stmt1->bind_result($story_title, $story_content);
        $stmt1->fetch();
        $stmt1->close();
    ?>
<div class="headerContainer">
        <h3 class="logo"> <a href="home.php">Community News </a> </h3>
        
        <p class="welcome"> Welcome,
        
            <?php
                echo $_SESSION['user'];
            ?>
        
        </p>
        
        <form id="search" class="searchForm" action="searchResults.php" method="POST">
                <label class="searchLabel">
                    Search For:
                </label><br>
                <input type="text" class="searcharea" name="SearchFor"/>
                <input form="search" type="submit" value="Search"/>
        </form>        
        
        <div id="menuLinksContainer">        
            <a class="menuLinks" href="newpost.php">New Post</a>
            
            <a class="menuLinks" href="mystories.php">My Stories</a> 
            
            <a class="menuLinks" class="lastmenuLinks" href="logout.php">Log out</a>
        </div>

</div>

<div class="bodyContainer">

        <form class="editStoryForm" id="editStory" action="story_editor.php" method="POST">
            <label>
                Edit your title:
            </label><br>
            <textarea form="editStory" name="title" class="newpostarea"><?php echo $story_title;?></textarea></br>    
            <label>
                Edit your story:
            </label><br>
            <textarea form="editStory" class="newpostarea" name="body"><?php echo $story_content;?></textarea></br>
            <input type="hidden" name="story_id" value="<?php echo $story_id;?>"/>
            
           <div class="right">
                <input form="editStory" type="submit" value="Submit Changes"/>
           </div>
        </form>

</div>

<div class="footer">
        
    <a href="deactivate.php">Delete account</a>
</div>

</body>

</html>