<?php
    require 'databaseAccess.php';

    session_start();
    
    $title = htmlentities($_POST['title']);
    $body = htmlentities($_POST['body']);
    $author = $_SESSION['user'];
     
    $stmt = $mysqli->prepare("insert into stories (title, body, author) values (?, ?, ?)");
    $stmt->bind_param('sss', $title, $body, $author);
    $stmt->execute();
    
        //story is added to database
        
        header("Location: home.php");
        exit;
?>