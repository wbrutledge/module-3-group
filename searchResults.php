<!DOCTYPE html>
<head>
    <title>Community News - Search</title>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen"/>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Cantora+One' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'/>
</head>
<body>
    <?php
        require 'databaseAccess.php';

        session_start();
        if (empty($_SESSION['user'])){
            header("Location: index.php?loginErrorWarning=q");
            exit;
        }
        $keyword = $_POST['SearchFor'];
    ?>
<div class="headerContainer">
        <h3 class="logo"> <a href="home.php">Community News </a> </h3>
        
        <p class="welcome"> Welcome,
        
            <?php
                echo $_SESSION['user'];
            ?>
        
        </p>
        
        <form id="search" class="searchForm" action="searchResults.php" method="POST">
                <label class="searchLabel">
                    Search For:
                </label><br>
                <input type="text" class="searcharea" name="SearchFor"/>
                <input form="search" type="submit" value="Search"/>
        </form>
                    
        <div id="menuLinksContainer">
                                
            <a class="menuLinks" href="newpost.php">New Post</a>
            
            <a class="menuLinks" href="mystories.php">My Stories</a> 
            
            <a class="menuLinks" class="lastmenuLinks" href="logout.php">Log out</a>
        </div>

</div>

<div class="bodyContainer">
<!--Create list of story titles that fit the search parameters-->
    <?php
        require "databaseAccess.php";
        $stmt = $mysqli->prepare("select title, author, body, time, id from stories where title LIKE ? OR body LIKE ?");
            $keywordX = "%".$keyword."%";
            $stmt->bind_param('ss', $keywordX, $keywordX);
            $stmt->execute();
            $stmt->bind_result($title, $author, $body, $time, $story_id);

        while ($stmt->fetch()){
            echo "<div class='storySnippet'>";
            echo "<a href=story.php?id=".$story_id.">";
                echo "<div class='storyTitle'>".$title."</div>";
                echo "<div class='storyAuthor'>".$author."</div>";
                echo "<div class='storyTime'>".$time."</div>";
                //truncate body to 300 character snippet
                $body_trunc = substr($body, 0, 300)."...";
                echo "<div class='storyBody'>".$body_trunc."</div>";
            echo "</a>";
            echo "</div>";

        }
        $stmt->close();
        
        
    ?>

</div>



