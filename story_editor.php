<?php
    require 'databaseAccess.php';
    
    $story_title = htmlentities($_POST['title']);
    $story_content = htmlentities($_POST['body']);
    $story_id = $_POST['story_id'];
    
    $stmt1 = $mysqli->prepare("update stories set title=?, body=? where id=?");
        $stmt1->bind_param('ssi', $story_title, $story_content, $story_id);
        $stmt1->execute();
        $stmt1->close();
   
    $path = "story.php?id=".$story_id;
            header("Location: $path");
            exit;

?>