<?php
    require 'databaseAccess.php';
    
    $comment_id = $_POST['comment_id'];
    
    $stmt1 = $mysqli->prepare("select story_id from comments where comment_id=?");
        $stmt1->bind_param('i', $comment_id);
        $stmt1->execute();
        $stmt1->bind_result($story_id);
        $stmt1->fetch();
        $stmt1->close();
        
    $stmt2 = $mysqli->prepare("delete from comments where comment_id=?");
        $stmt2->bind_param('i', $comment_id);
        $stmt2->execute();
            
    $path = "story.php?id=".$story_id;
            header("Location: $path");
            exit;

?>