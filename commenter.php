<?php
    require 'databaseAccess.php';

    session_start();
    
    $story_id = $_POST['story_id'];
    $comment_content = htmlentities($_POST['comment']);
    $author = $_SESSION['user'];
     
    $stmt = $mysqli->prepare("insert into comments (comment, author, story_id) values (?, ?, ?)");
    $stmt->bind_param('sss', $comment_content, $author, $story_id);
    $stmt->execute();
    
        //comment is added to database, return to same story page
        $path = "story.php?id=".$story_id;
            header("Location: $path");
            exit;
?>