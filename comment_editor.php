<?php
    require 'databaseAccess.php';
    
    $comment_content = htmlentities($_POST['body']);
    $comment_id = $_POST['comment_id'];
    $story_id = $_POST['story_id'];
    
    $stmt1 = $mysqli->prepare("update comments set comment=? where comment_id=?");
        $stmt1->bind_param('si', $comment_content, $comment_id);
        $stmt1->execute();
        $stmt1->close();
   
    $path = "story.php?id=".$story_id;
            header("Location: $path");
            exit;

?>