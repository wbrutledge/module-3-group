<!DOCTYPE html>
<head>
    <title>Community News - Story</title>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen"/>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Cantora+One' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'/>
</head>
<body>
<?php
    session_start();
    require "databaseAccess.php";
    $story_id = $_GET['id'];
    $stmt = $mysqli->prepare("select title, body, author, time from stories where id=?");
    $stmt->bind_param('i', $story_id);
    $stmt->execute();
    $stmt->bind_result($title, $body_content, $author, $time);
    $stmt->fetch();
?>

<div class="headerContainer">
        <h3 class="logo"> <a href="home.php">Community News </a> </h3>
        
        <p class="welcome">
        
            <?php
                if (!empty($_SESSION['user'])){
                    echo "Welcome, ".$_SESSION['user'];
                }
            ?>
        
        </p>
        
        <form id="search" class="searchForm" action="searchResults.php" method="POST">
                <label class="searchLabel">
                    Search For:
                </label><br>
                <input type="text" class="searcharea" name="SearchFor"/>
                <input form="search" type="submit" value="Search"/>
        </form>
        
        <?php
            if (!empty($_SESSION['user'])){
                echo "<div id='menuLinksContainer'>        
                        <a class='menuLinks' href='newpost.php'>New Post</a>
                        <a class='menuLinks' href='mystories.php'>My Stories</a> 
                        <a class='menuLinks' class='lastmenuLinks' href='logout.php'>Log out</a>
                    </div>";
            }
            else{
                echo "<div id='menuLinksContainer'>
                        <a class='menuLinks' href='index.php'>Log In</a>
                      </div>";   
            }
        ?>

</div>


<!-- STORY CONTENT -->
<div class="bodyContainer">
    <div class="storyTitleWrap"> <?php
            echo "<div class='storyTitle'>".$title."</div>";
            echo "<div class='storyAuthor'>".$author."</div>";
            echo "<div class='storyTime'>".$time."</div>";
            
            if(@$_SESSION['user']==$author){ 
                //The current user is the author of this story. Display buttons to edit and delete the story
                echo "<form id='editStory' class='editdelete' method='POST' action='editStory.php'>
                        <input type='hidden' name='story_id' value='".$story_id."'></input>
                        <input type='submit' value='Edit'></input>
                      </form>";
                 echo "<form id='deleteStory' class='editdelete' method='POST' action='deleteStory.php'>
                        <input type='hidden' name='story_id' value='".$story_id."'></input>
                        <input type='submit' value='Delete'></input>
                      </form>";
            }
        ?>
    </div>
    
    <p class="newsContent">
        <?php
             echo "<div class='storyBody'>".$body_content."</div>";
             //if user is logged in and the current user is the author of the story, create links to edit/delete.
        
        ?>
    </p>


<br><br>
<!-- COMMENTS -->
<div class="commentsContainer">
    <?php
        require "databaseAccess.php";
        //get all comments for this story ID.
        $stmt = $mysqli->prepare("insert into comments (comment, author, story_id) values (?, ?, ?)");

        $stmt = $mysqli->prepare("select comment, author, time, comment_id from comments where story_id=? group by time");
            //echo $story_id;
        $stmt->bind_param('s', $story_id);
        $stmt->execute();
        $stmt->bind_result($comment_content, $comment_author, $comment_time, $comment_id);
        while ($stmt->fetch()){
            echo "<div class='commentWrap'>";
            echo "<div class='commentAuthor'>".$comment_author."</div>";
            echo "<div class='commentTime'>".$comment_time."</div>";
            if (@ $_SESSION['user']==$comment_author){
                //This is a comment by the current user. Add buttons to edit or delete comment
                echo "<form id='editComment' class='editdelete' method='POST' action='editComment.php'>
                        <input type='hidden' name='comment_id' value='".$comment_id."'></input>
                        <input type='hidden' name='story_id' value='".$story_id."'></input>
                        <input type='submit' value='edit'></input>
                      </form>";
                echo "<form id='deleteComment' class='editdelete' method='POST' action='deleteComment.php'>
                        <input type='hidden' name='comment_id' value='".$comment_id."'></input>
                        <input type='submit' value='delete'></input>
                      </form>";
            }
            echo "<div class='commentComment'>".$comment_content."</div>";
            echo "</div>";
            
        }
        $stmt->close();
        
        
    ?>
</div>

<br><br>
<!--IF THE USER IS LOGGED IN, DISPLAY A FORM TO COMMENT-->

<?php
    if (!empty($_SESSION['user'])){
        echo"<form id='newcomment' action='commenter.php' method='POST'>
                <textarea form='newcomment' name='comment' rows='5'></textarea><br>
                <input type='submit' value='Post Comment'/>
                <input type='hidden' name='story_id' value='$story_id'/>
            </form>";
    }
?>

</div>

<div class="footer">
        
    <a href="deactivate.php">Delete account</a>
</div>

</body>

</html>